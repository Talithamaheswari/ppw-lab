from django.urls import re_path
from .views import *
#url for app
urlpatterns = [
    re_path(r'about-me/', aboutme, name='aboutme'),
    re_path(r'education/', education, name='education'),
    re_path(r'experience/', experience, name='experience'),
    re_path(r'intro/', intro, name='intro'),
    re_path(r'skills/', skills, name='skills'),
    re_path(r'home/', home, name='home'),
    re_path(r'swot/', swot, name='swot'),
    re_path(r'schedules/', schedules, name='schedules'),
    re_path(r'post_schedule/', schedule_post, name='schedule_post'),
    re_path(r'view_schedules/', display_schedules, name ='display_schedule'),
]
