from django.shortcuts import render
from datetime import datetime, date
from .forms import *
from .models import JadwalPribadi
response = {'author': "Talitha"}
# Enter your name here
# Create your views here.


def index(request):
    return render(request, 'index_lab1.html')

def aboutme(request):
	return render(request, 'abtme.html')

def education(request):
	return render(request, 'education1.html')

def experience(request):
	return render(request, 'experience.html')

def intro(request):
	return render(request, 'pagepertama.html')

def skills(request):
	return render(request, 'skills.html')

def home(request):
	return render(request, 'homepage.html')

def swot(request):
	return render(request, 'swot.html')

def schedules(request):
    response['schedule_form'] = Schedule_Form
    return render(request, 'schedules.html', response)

def display_schedules(request):
    semuajadwal = JadwalPribadi.objects.all()
    response['schedule'] = semuajadwal
    html = 'schedules_result.html'
    return render(request,html ,response)

def schedule_post(request):
    form = Schedule_Form(request.POST or None)
    if (request.method == 'POST' and 'submit' in request.POST):
        response['kegiatan'] = request.POST['kegiatan']if request.POST['kegiatan']!="" else "Anonymous"
        response['waktu'] = request.POST['waktu']if request.POST['waktu']!="" else "Anonymous"
        response['tempat'] = request.POST['tempat']if request.POST['tempat']!="" else "Anonymous"
        response['kategori'] = request.POST['kategori']
        jadwalpribadi = JadwalPribadi(kegiatan=response['kegiatan'], waktu=response['waktu'], tempat=response['tempat'], kategori=response['kategori'])
        jadwalpribadi.save()
        semuajadwal = JadwalPribadi.objects.all()
        response['schedule'] = semuajadwal
        html = 'schedules_result.html'
        return render(request, html, response)
    elif (request.method == 'POST' and 'delete' in request.POST):
        html = 'schedules_result.html'
        semuajadwal = JadwalPribadi.objects.all()
        response['schedule'] = semuajadwal.delete()
        return render(request,html, response)
    else:
        semuajadwal = JadwalPribadi.objects.all()
        response['schedule'] = semuajadwal
        html = 'schedules_result.html'
        return render(request, html, response)

